module.exports = function(grunt) {

	grunt.initConfig({

    paths: {

      scripts: {
        src: ['public/js/main.js'],
        dest: 'public/js/main.min.js'
      },

      styles: {
        sass: {
          src: 'public/sass/**/*.{scss, sass}',
          dest: 'public/sass/'
        },
        css: {
          src: 'public/css/**/*.css',
          dest: 'public/css/'
        }
      },

      images: {
        src: 'public/img/'
      }

    },

    watch: {

      options: {
        livereload: true
      },
      
      scripts: {
        files: '<%= paths.scripts.src %>',
        tasks: ['jshint']
      },

      scss: {
        files: '<%= paths.styles.sass.src %>',
        tasks: ['compass']
      }

    },

    connect: {
      server: {
        options: {
          port: 9001,
          hostname: '0.0.0.0',
          livereload: true,
          open: true
        }
      }
    },

    compass: {
      dist: {
        options: {
          sassDir: '<%= paths.styles.sass.dest %>',
          cssDir: '<%= paths.styles.css.dest %>',
          imagesDir: '<%= paths.images.src %>',
          relativeAssets: true
        }
      }
    },

    jshint: {
      all: '<%= paths.scripts.src %>'
    },

    uglify: {
      my_target: {
        files: {
          '<%= paths.scripts.dest %>': '<%= paths.scripts.src %>'
        }
      }
    },

    cssmin: {
      minify: {
        expand: true,
        cwd: '<%= paths.styles.css.dest %>',
        src: ['*.css', '!*.min.css'],
        dest: '<%= paths.styles.css.dest %>',
        ext: '.min.css'
      }
    },

    imagemin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= paths.images.src %>',
          src: ['*.{png, jpg, gif}'],
          dest: '<%= paths.images.src %>'
        }]
      }
    }

	});

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-connect');

  grunt.registerTask('default', ['connect', 'watch']);
  grunt.registerTask('build', ['cssmin', 'uglify', 'imagemin']);

};