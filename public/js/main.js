$(function() {
	'use strict';

  var numbers = [];

	var getRandomNumber = function(min, max) {
    var number = Math.floor(Math.random() * (max - min + 1)) + min;

    if(numbers.length < max) {
      if(numbers.indexOf(number) === -1) {
        numbers.push(number);

        return number;
      } else {
        return getRandomNumber(min, max);
      }
    } else {
      alert('Todos os números foram sorteados');
    }
  };

  $('.getRandomNumber').on('click', function() {
    var items = [];
    var number = getRandomNumber(1, 75);

    numbers.sort(function(a, b) {
      return a - b;
    });

    numbers.forEach(function(elem) {
      items.push('<span>' + elem + '</span>');
    });

    if($('.currentNumber').hasClass('start')) {
      $('.currentNumber').removeClass('start');
    }
    
    $('.currentNumber').html(number);
    $('.allNumbers').html(items);
  });

  $('.newGame').on('click', function() {
    location.reload();
  });

});