cocada
======

####Requisitos

+ Compass
+ Sass
+ npm
+ bower

####Dependências

Para instalar as dependências de **desenvolvimento**, rode:
```shell
  sudo npm install
```

Para instalar as dependências de **produção**, rode:
```shell
  bower install
```

Vale lembrar que você deve **instalar as dependências de produção mesmo quando em desenvolvimento**.

####Utilização

Para compilar arquivos em **desenvolvimento**, rode:
```shell
  grunt
```

Para compilar arquivos para **produção**, rode:
```shell
  grunt build
```

####Implementação do back-end

+ É mais seguro utilizar os arquivos **base.html** e **index.html** para recortar estruturas padrões, como **header**, **footer**, **sidebar**, etc.
+ Estes arquivos contém estas e outras estruturas padrões do projeto, principalmente o arquivo **base.html**

####Considerações

+ Ao subir para produção, não se esqueça de apontar o HTML para os arquivos CSS e JS minificados. Eles estão, respectivamente em **/public/css/main.min.css** e **/public/js/main.min.js**
+ A pasta **/public/img** deve conter todas as imagens obrigatórias
+ A pasta **/public/img/samples** deve conter todas as imagens de exemplo
+ A pasta **/public/img/sprites** deve conter todas as imagens que serão compiladas na sprite
+ Para usar uma imagem compilada na sprite utilize uma classe como **.sprites-nome-da-imagem**
+ Vale lembrar que apenas arquivos do tipo PNG serão compilados na sprite
+ Alguns campos do tipo hidden estão sendo manipulados via JavaScript, principalmente em formulários que trabalham com **selects**